public interface OneVariableFunctionOnSection {
    double FunctionCount(double x);
    double getSectionLeft();
    double getSectionRight();
}
