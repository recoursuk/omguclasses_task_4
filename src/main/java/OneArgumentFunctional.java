public interface OneArgumentFunctional<T extends OneVariableFunctionOnSection> {
    double FunctionalCount(T function);
}
