public class FunctionSumOnSection<T extends OneVariableFunctionOnSection> implements OneArgumentFunctional<T>{

    @Override
    public double FunctionalCount(T function) {
        return function.FunctionCount(function.getSectionLeft()) +
                function.FunctionCount(function.getSectionRight())+
                function.FunctionCount((function.getSectionRight()+function.getSectionLeft())/2);

    }
}
