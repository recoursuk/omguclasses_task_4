import static java.lang.StrictMath.exp;

public class ExponentialFunction extends Function {
    private double A,B;
    public ExponentialFunction(double a, double b, double A, double B){
        super(a,b);
        this.A = A;
        this.B = B;
    }

    @Override
    public double FunctionCount(double x){
        if(!xInSection(x)){
            throw new IllegalArgumentException("x not in section");
        }
        return this.A*exp(x)+this.B;
    }
}
