public class FractionFunction extends Function {
    private double A,B,C,D;
    public FractionFunction(double a,double b, double A, double B, double C, double D){
        super(a,b);
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
    }

    @Override
    public double FunctionCount(double x) {
        if(!xInSection(x)){
            throw new IllegalArgumentException("x not in Section");
        }
        if(C*x + D == 0){
            throw new IllegalArgumentException("Division by 0");
        }
        return (A*x+B)/(C*x+D);
    }
}
