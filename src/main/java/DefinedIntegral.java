public class DefinedIntegral<T extends OneVariableFunctionOnSection> implements OneArgumentFunctional<T> {
    private double a,b;
    public DefinedIntegral(double a,double b){
        this.a = a;
        this.b = b;
    }

    private double getIntegral(int countOfRects, T function){
        double integral = 0;
        for (int i = 0; i<countOfRects; i++){
            try {
                integral += function.FunctionCount(a + i * b / countOfRects) * ((b - a) / countOfRects);
            }catch (Exception ignored){
            }
        }
        return integral;
    }


    @Override
    public double FunctionalCount(T function) {
        if(function.getSectionLeft() > a || function.getSectionRight() < b){
            throw new IllegalArgumentException("Illegal Section");
        }
        return getIntegral(1000, function);
    }
}
