public class LinearFunction extends Function {
    private double A,B;
    public LinearFunction(double a, double b, double A, double B){
        super(a,b);
        this.A = A;
        this.B = B;
    }

    @Override
    public double FunctionCount(double x) {
        if (!xInSection(x)) {
            throw new IllegalArgumentException("X not in Section");
        }
        return this.A*x+this.B;
    }
}
