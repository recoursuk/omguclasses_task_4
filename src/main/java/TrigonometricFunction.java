import static java.lang.StrictMath.exp;
import static java.lang.StrictMath.sin;

public class TrigonometricFunction extends Function{
    private double A,B;
    public TrigonometricFunction(double a, double b, double A, double B){
        super(a,b);
        this.A = A;
        this.B = B;
    }

    @Override
    public double FunctionCount(double x){
        if(!xInSection(x)){
            throw new IllegalArgumentException("x not in section");
        }
        return this.A*sin(this.B*x);
    }
}
