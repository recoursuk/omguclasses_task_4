abstract public class Function implements OneVariableFunctionOnSection{
    private double a,b;
    public Function(double a, double b){
        this.a = a;
        this.b = b;
    }

    @Override
    public double getSectionLeft() {
        return a;
    }

    @Override
    public double getSectionRight() {
        return b;
    }

    @Override
    public double FunctionCount(double x){
        return 0;
    }

    public static boolean inSection(OneVariableFunctionOnSection function, double a, double b){
        if (function.getSectionLeft() > a || function.getSectionRight() < b) {
            return false;
        }
        return true;
    }
    public boolean xInSection(double x){
        if (x<this.a || x>this.b){
            return false;
        }
        return true;
    }

}
