import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FractionFunctionTest {
    @Test
    void FunctionCount() {
        Function f = new FractionFunction(-10,10,1,5,1,-7);
        Assertions.assertThrows(IllegalArgumentException.class, () -> f.FunctionCount(17));
        Assertions.assertEquals(f.FunctionCount(1),-1,1e-9);
    }
}
