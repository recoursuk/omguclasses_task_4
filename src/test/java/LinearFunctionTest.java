import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LinearFunctionTest {

    @Test
    void FunctionCount() {
        Function f = new LinearFunction(-10,10,1,0);
        Assertions.assertThrows(IllegalArgumentException.class,() -> f.FunctionCount(11));
        Assertions.assertEquals(f.FunctionCount(5),5,1e-10);
    }
}
