
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class IntegralOnSegmentTest {
    @Test
    void getMappingExp() {
        OneArgumentFunctional<ExponentialFunction> f = new DefinedIntegral<ExponentialFunction>(0,3);
        Assertions.assertEquals(f.FunctionalCount(new ExponentialFunction(0,100,1,0)),19.086,1e-1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> f.FunctionalCount(new ExponentialFunction(0, 1, 1, 0)));
    }

    @Test
    void getMappingLinear(){
        OneArgumentFunctional<LinearFunction> f = new DefinedIntegral<>(0,10);
        Assertions.assertEquals(f.FunctionalCount(new LinearFunction(0,100,5,5)),300,1e+1);
    }

    @Test
    void getMappingSinus(){
        OneArgumentFunctional<TrigonometricFunction> f = new DefinedIntegral<>(0,10);
        Assertions.assertEquals(f.FunctionalCount(new TrigonometricFunction(0,100,1,1)),1.8391,1e-1);
    }

    @Test
    void getMappingFractionLinear(){
        OneArgumentFunctional<FractionFunction> f = new DefinedIntegral<>(0,10);
        //Assertions.assertThrows(IllegalArgumentException.class , () ->f.FunctionalCount(new FractionFunction(-100,100,1,5,7,-7)));
        Assertions.assertEquals(f.FunctionalCount(new FractionFunction(-100,100,1,5,7,-7)),3.31,1e-1);
        //Assertions.assertThrows(IllegalArgumentException.class, () -> f.FunctionalCount(new FractionFunction(-100,100,1,1,1,2)));
    }
}
