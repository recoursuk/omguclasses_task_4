import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class TrigFunctionTest {
    @Test
    void FunctionCount() {
        Function f = new TrigonometricFunction(-10,10,1,0);
        Assertions.assertThrows(IllegalArgumentException.class,() -> f.FunctionCount(11));
        Assertions.assertEquals(f.FunctionCount(5),0,1e-10);
    }
}
