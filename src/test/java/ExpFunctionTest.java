import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExpFunctionTest {
    @Test
    void FunctionCount() {
        Function f  = new ExponentialFunction(-10,10,1,0);
        Assertions.assertThrows(IllegalArgumentException.class , () -> f.FunctionCount(11));
        Assertions.assertEquals(f.FunctionCount(1),Math.E,1e-10);
    }
}
